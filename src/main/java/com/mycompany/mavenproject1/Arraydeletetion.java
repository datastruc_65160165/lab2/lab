/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */


import java.util.Arrays;

/**
 *
 * @author Tauru
 */
public class Arraydeletetion {

    static int[] deleteElementByIndex(int[] arr, int index) {
        int[]copyArr = new int[arr.length-1];
        for (int i = 0,k =0; i < arr.length; i++) {
            if(i==index){
                continue;
            }
            copyArr[k++] = arr[i];
        }
        return copyArr;
    }

    static int[] deleteElementByValue(int[] arr, int value) {
        int index = -1;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                index = i;
                break;
            }
        }
        
        if (index == -1) {
            return arr;
        }
        
        int[] updatedArr = new int[arr.length - 1];
        int j = 0;

        for (int i = 0; i < arr.length; i++) {
            if (i != index) {
                updatedArr[j] = arr[i];
                j++;
            }
        }
        return updatedArr;

    }

    public static void main(String[] args) throws Exception {
        int [] arr = {1, 2, 3, 4, 5} ;
        int index = 2;
        int value = 4;
        System.out.println("Original Array: "+ Arrays.toString(arr));
        arr = deleteElementByIndex(arr, index);
        System.out.printf("Array after deleting element at index "+index+" : "+ Arrays.toString(arr));
        System.out.println();
        arr = deleteElementByValue(arr, value);
        System.out.printf("Array after deleting element with value "+value+" : "+ Arrays.toString(arr));
    }
}

